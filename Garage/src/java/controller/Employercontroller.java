/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import employer.Employer;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import static java.lang.System.out;

/**
 *
 * @author MAMINIAINA
 */
public class Employercontroller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Employercontroller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Employercontroller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    /**
     *
     */
    public void getList(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        Employer[] employer=new Employer[5];
        employer[0]=new Employer();

        employer[0].setNom("Randria");
        employer[0].setGenre("Homme");
        employer[0].setNaissance ("12-02-1993");
        employer[0].setNivetude("Bacc");
        
        employer[1]=new Employer();
        
        employer[1].setNom("Rasoa");
        employer[1].setGenre("Femme");
        employer[1].setNaissance ("15-03-2000");
        employer[1].setNivetude("BEPC");
        
        employer[2]=new Employer();
        
        employer[2].setNom("Randria");
        employer[2].setGenre("Homme");
        employer[2].setNaissance ("15-03-1991");
        employer[2].setNivetude("Bacc");
        
        employer[3]=new Employer();
        
        employer[3].setNom("Jean");
        employer[3].setGenre("Homme");
        employer[3].setNaissance ("15-03-1992");
        employer[3].setNivetude("Licence");
        
        employer[4]=new Employer();
        
        employer[4].setNom("Jeanne");
        employer[4].setGenre("Femme");
        employer[4].setNaissance ("15-03-1995");
        employer[4].setNivetude("Licence");
        
        req.setAttribute("employer", employer);  

        RequestDispatcher dispat = req.getRequestDispatcher("/ListeEmployer.jsp");
        dispat.forward(req,res);
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param req servlet request
     * @param res servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        String etat=req.getParameter("etat");
        if(etat.equals("liste")){
            this.getList(req,res);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
