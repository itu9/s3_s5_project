/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employer;

import java.util.Date;

/**
 *
 * @author MAMINIAINA
 */
public class Employer {

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * @return the nivetude
     */
    public String getNivetude() {
        return nivetude;
    }

    /**
     * @param nivetude the nivetude to set
     */
    public void setNivetude(String nivetude) {
        this.nivetude = nivetude;
    }

    /**
     * @return the naissance
     */
    public String getNaissance() {
        return naissance;
    }

    /**
     * @param naissance the naissance to set
     */
    public void setNaissance(String naissance) {
        this.naissance = naissance;
    }
    private int id;
    private String nom;
    private String genre;
    private String nivetude;
    private String naissance;
    
}
